﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Railroad.Models
{
	public class Station
	{
		public int ID { get; set; }
		public string Name { get; set; }
		public string Symbol { get; set; }
		
	}
}