﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Railroad.Startup))]
namespace Railroad
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
