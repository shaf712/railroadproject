﻿using MySql.Data.MySqlClient;
using Railroad.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Railroad.Controllers
{
	public class HomeController : Controller
	{
		public ActionResult Index()
		{
			var stations = new List<Station>(); 
			DataSet ds = new DataSet();
			using (MySqlConnection conn = new MySqlConnection(ConfigurationManager.ConnectionStrings["DB"].ConnectionString))
			{
				MySqlCommand cmd = new MySqlCommand("GetStations", conn);
				cmd.CommandType = CommandType.StoredProcedure;
				MySqlDataAdapter adp = new MySqlDataAdapter(cmd);
				adp.Fill(ds); 
			}

			foreach (DataRow row in ds.Tables[0].Rows)
			{
				var station = new Station();
				station.ID = (int)row["station_id"];
				station.Name = row["station_name"].ToString();
				station.Symbol = row["station_symbol"].ToString();
				stations.Add(station); 
			}

			return View(stations);
		}

		public ActionResult About()
		{
			ViewBag.Message = "Your application description page.";

			return View();
		}

		public ActionResult Contact()
		{
			ViewBag.Message = "Your contact page.";

			return View();
		}
	}
}